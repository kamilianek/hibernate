package main.Model;

import javax.persistence.*;

import java.util.HashSet;
import java.util.Set;

@Entity
public class Supplier extends Company {

    @OneToMany(mappedBy = "supplier")
    private Set<Product> productsToBeSupplied;

    private String bankAccountNumber;

    public Supplier(){

    }

    public Supplier(String companyName, String street, String city, String zipCode, String bankAccountNumber){
        super(companyName, street, city, zipCode);
        this.bankAccountNumber = bankAccountNumber;
        productsToBeSupplied = new HashSet<>();
    }


    public void addProductToBeSupplied(Product product){
        this.productsToBeSupplied.add(product);
        product.setSupplier(this);
    }

    public Set<Product> getProductsToBeSupplied() {
        return productsToBeSupplied;
    }

    @Override
    public String toString(){
        return super.toString() + " bankAccountNumber: " + this.bankAccountNumber;
    }
}
