package main.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int transactionNumber;

    private int quantity;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<Product> products;

    @Override
    public String toString(){
        return "transactionNumber: " + this.transactionNumber + " quantity: " + this.quantity;
    }

    public int getId(){
        return this.transactionNumber;
    }

    public Transaction(){

    }

    public Transaction(int quantity){
        this.quantity = quantity;
        this.products = new HashSet<>();
    }

    public Set<Product> getProducts(){
        return products;
    }

    public void addProduct(Product product){
        this.products.add(product);
        product.getTransactions().add(this);
    }



}
