package main.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String productName;
    private int unitsOnStock;

    @ManyToOne
    @JoinColumn(name = "SUPPLIER_ID")
    private Supplier supplier;

    @ManyToMany(mappedBy = "products", cascade = CascadeType.PERSIST)
    private Set<Transaction> transactions;

    public Product(){
    }

    public Product(String productName, int unitsOnStock){
        this.productName = productName;
        this.unitsOnStock = unitsOnStock;
        this.transactions = new HashSet<>();
    }

    public void setSupplier(Supplier supplier){
        this.supplier = supplier;
    }

    public Supplier getSupplier(){
        return this.supplier;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public int getId(){
        return this.id;
    }


    @Override
    public String toString() {
        return String.format("ID: %d, Name: %s, Units: %d",
                id, productName, unitsOnStock);
    }
}
