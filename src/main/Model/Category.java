package main.Model;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int categoryId;

    private String name;

    @OneToMany
    @JoinColumn(name = "CATEGORY_ID")
    private List<Product> products;

    @Override
    public String toString(){
        return "id: " + categoryId + " name: " + name;
    }

    public Category(){

    }

    public Category(String name){
        this.name = name;
        this.products = new LinkedList<>();
    }

    public void addProduct(Product product){
        this.products.add(product);
    }

}
