import main.Model.Customer;
import main.Model.Product;
import main.Model.Supplier;
import main.Model.sessionconf.SessionManager;
import org.hibernate.*;




public class Main {
    public static void main(final String[] args) throws Exception {

        final Session session = SessionManager.getSession();
        Transaction tx = session.beginTransaction();


        Supplier supplier1 = new Supplier("ABC", "Nawojki", "Kraków", "31-126","4353 6723 0000 0234 2340");
        Supplier supplier2 = new Supplier("XYZ", "Czarnowiejska", "Kraków", "31-126", "12300 0000 2341 3245 0301");

        session.save(supplier1);
        session.save(supplier2);

        Customer customer1 = new Customer("Compan", "Kawiory", "Kraków", "31-126", 33.3);
        Customer customer2 = new Customer("Companin", "Czarnowiejska", "Kraków", "31-126", 15.0);

        session.save(customer1);
        session.save(customer2);

        tx.commit();
        session.close();

    }
}